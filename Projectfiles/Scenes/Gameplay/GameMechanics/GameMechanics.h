//
//  GameMechanics.h
//  _MGWU-SideScroller-Template_
//
//  Created by Benjamin Encz on 5/17/13.
//  Copyright (c) 2013 MakeGamesWithUs Inc. Free to use for all purposes.
//

#import <Foundation/Foundation.h>
#import "Knight.h"
#import "Game.h"
#import "GameplayLayer.h"


// define the different possible GameState types, by using constants
typedef NS_ENUM(NSInteger, GameState) {
    GameStatePaused,
    GameStateMenu,
    GameStateRunning
};

/**
 This class stores several global game parameters. It stores 
 references to several entities and sets up e.g. the 'floorHeight'.
 
 This class is used by all entities in the game to access shared ressources.
**/

#define SCROLL_SPEED_DEFAULT 60.f
#define SCROLL_SPEED_SKIP_AHEAD 200.0f
#define KNIGHT_HIT_POINTS 1
#define DEFAULT_GAME_SPEED 3.0f
#define MAX_GAME_SPEED 15.0f
#define DEFAULT_powerUpDuration 100
#define DEFAULT_life 1
#define DEFAULT_spawnRate 1.0f
#define DEFAULT_sensitivity 400.0f
#define DEFAULT_maxVelocity 700.0f
#define DEFAULT_changeLaneRate 60.0f


@interface GameMechanics : NSObject

// determines the current state the game is in. Either menu mode (scene displayed beyond main menu) or gameplay mode.
@property (nonatomic, assign) GameState gameState;

// reference to the main character
@property (nonatomic, weak) Knight *knight;

// reference to the current game object
@property (nonatomic, weak) Game *game;

// reference to the GamePlay-Scene
@property (nonatomic, weak) GameplayLayer *gameScene;

// sets up a gravity (used for jumping mechanism)
@property (nonatomic, assign) CGPoint worldGravity;

// sets a floor height (entities cannot move below)
@property (nonatomic, assign) float floorHeight;

// stores the scroll speed of the background (that speed influences speed of knight and enemies)
@property (nonatomic, assign) float backGroundScrollSpeedX;

// stores the individual spawn rates for all monster types
@property (nonatomic, strong, readonly) NSMutableDictionary *spawnRatesByMonsterType;

// sets game speed
@property (nonatomic, assign) float GAME_SPEED;

// distance traveled
@property (nonatomic, assign) float distance;

// previous distance
@property (nonatomic, assign) float prevDist;

// when powerup / power down was picked up
@property (nonatomic, assign) int pick;
@property (nonatomic, assign) int pickPD;

// trip if a powerup is picked up
@property (nonatomic, assign) BOOL speedUp;

// current game speed that will continuously increase until MAX_GAME_SPEED
@property (nonatomic, assign) float currentGS;

// to speed down
@property (nonatomic, assign) BOOL speedDown;

// define what powerup/powerdown is used
@property (nonatomic, assign) int powerUpType;
@property (nonatomic, assign) int powerDownType;

//powerup (0:none,1:mech,2:teleport)
//powerdown (0:none,1:oil)

// global spawn rate
@property (nonatomic, assign) float spawnRate;

// test if passed 250 meters
@property (nonatomic, assign) BOOL passed250;

// initial life for user, update if bought upgrade
@property (nonatomic, assign) int life;

// length of power up
@property (nonatomic, assign) float powerUpDuration;

//rate of cars changing lane
@property (nonatomic, assign) int changeLaneRate;

// max sensitivity and vel for accelerometer
@property (nonatomic, assign) float sensitivity;
@property (nonatomic, assign) float maxVel;

// bool to keep track if powered up and powered down
@property (nonatomic, assign) BOOL poweredUp;
@property (nonatomic, assign) BOOL poweredDown;

// parallax background change
@property (nonatomic, assign) BOOL bgChange;

// check if in tunnel
@property (nonatomic, assign) BOOL inTunnel;

@property (nonatomic, assign) BOOL oilHit;

// for tunnel use
@property (nonatomic, assign) BOOL lane1taken;
@property (nonatomic, assign) BOOL lane2taken;
@property (nonatomic, assign) BOOL lane3taken;


// gives access to the shared instance of this class
+ (id)sharedGameMechanics;


- (void)setSpawnRate:(int)spawnRate forMonsterType:(Class)monsterType;
- (int)spawnRateForMonsterType:(Class)monsterType;

// Resets the complete State of the sharedGameMechanics. Should be called whenever a new game is started.
- (void)resetGame;

@end
