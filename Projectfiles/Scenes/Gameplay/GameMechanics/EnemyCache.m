//
//  EnemyCache.m
//  _MGWU-SideScroller-Template_
//
//  Created by Benjamin Encz on 5/17/13.
//  Copyright (c) 2013 MakeGamesWithUs Inc. Free to use for all purposes.
//

#import "EnemyCache.h"
#import "BasicMonster.h"
#import "GameMechanics.h"
#import "Knight.h"
#import "speedup.h"
#import "GameplayLayer.h"

#define ENEMY_MAX 10

@implementation EnemyCache

+ (id)sharedEnemyCache
{
    static dispatch_once_t once;
    static id sharedInstance;
    /*  Uses GCD (Grand Central Dispatch) to restrict this piece of code to only be executed once
     This code doesn't need to be touched by the game developer.
     */
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+(id) cache
{
	id cache = [[self alloc] init];
	return cache;
}

- (void)dealloc
{
    /* 
     When our object is removed, we need to unregister from all notifications.
     */
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(id) init
{
	if ((self = [super init]))
	{
        // load all the enemies in a sprite cache, all monsters need to be part of this sprite file
        // currently the knight is used as the only monster type
        CCSpriteFrameCache* frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
		[frameCache addSpriteFramesWithFile:@"monster-animations.plist"];
        // we need to initialize the batch node with one of the frames
		CCSpriteFrame* frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"monster1_1.png"];
        /* A batch node allows drawing a lot of different sprites with on single draw cycle. Therefore it is necessary,
           that all sprites are added as child nodes to the batch node and that all use a texture contained in the batch node texture. */
		batch = [CCSpriteBatchNode batchNodeWithTexture:frame.texture];
		[self addChild:batch];
        [self scheduleUpdate];
        enemies = [[NSMutableDictionary alloc] init];
        
        /**
         A Notification can be used to broadcast an information to all objects of a game, that are interested in it.
         Here we sign up for the 'GamePaused' and 'GameResumed' information, that is broadcasted by the GameMechanics class. Whenever the game pauses or resumes, we get informed and can react accordingly.
         **/
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gamePaused) name:@"GamePaused" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameResumed) name:@"GameResumed" object:nil];
	}
	
	return self;
}

- (void)gamePaused
{
    // first pause this CCNode, then pause all monsters
    
    [self pauseSchedulerAndActions];
    
    for (id key in [enemies allKeys])
    {
        NSArray *enemiesOfType = [enemies objectForKey:key];
        
        for (BasicMonster *monster in enemiesOfType)
        {
            [monster pauseSchedulerAndActions];
        }
    }
}

- (void)gameResumed
{
    // first resume this CCNode, then pause all monsters

    [self resumeSchedulerAndActions];
    
    for (id key in [enemies allKeys])
    {
        NSArray *enemiesOfType = [enemies objectForKey:key];
        
        for (BasicMonster *monster in enemiesOfType)
        {
            [monster resumeSchedulerAndActions];
        }
    }
}

-(void) spawnEnemyOfType:(Class)enemyTypeClass
{
    /* the 'enemies' dictionary stores an array of available enemies for each enemy type.
     We use the class of the enemy as key for the dictionary, to receive an array of all existing enimies of that type.
     We use a CCArray since it has a better performance than an NSArray. */
	CCArray* enemiesOfType = [enemies objectForKey:enemyTypeClass];
    BasicMonster* enemy;
    
    /* we try to reuse existing enimies, therefore we use this flag, to keep track if we found an enemy we can
     respawn or if we need to create a new one */
    BOOL foundAvailableEnemyToSpawn = FALSE;
    
    // if the enemiesOfType array exists, iterate over all already existing enemies of the provided type and check if one of them can be respawned
    if (enemiesOfType != nil)// && [[GameMechanics sharedGameMechanics] inTunnel] == FALSE)
    {
        CCARRAY_FOREACH(enemiesOfType, enemy)
        {
            // find the first free enemy and respawn it
            if (enemy.visible == NO)
            {
                [enemy spawn];
                float GS = [[GameMechanics sharedGameMechanics] GAME_SPEED];
                enemy.velocity = CGPointMake(enemy.velocity.x, enemy.velocity.y-GS*3);
                // remember, that we will not need to create a new enemy
                foundAvailableEnemyToSpawn = TRUE;
                break;
            }
        }
    } else {
        // if no enemies of that type existed yet, the enemiesOfType array will be nil and we first need to create one
        enemiesOfType = [[CCArray alloc] init];
        [enemies setObject:enemiesOfType forKey:(id<NSCopying>)enemyTypeClass];
    }
    
    // if we haven't been able to find a enemy to respawn, we need to create one
    if (!foundAvailableEnemyToSpawn)// && [[GameMechanics sharedGameMechanics] inTunnel] == FALSE)
    {
        // initialize an enemy of the provided class
        BasicMonster *enemy =  [(BasicMonster *) [enemyTypeClass alloc] initWithMonsterPicture];
        [enemy spawn];
        [enemiesOfType addObject:enemy];
        float GS = [[GameMechanics sharedGameMechanics] GAME_SPEED];
        enemy.velocity = CGPointMake(enemy.velocity.x, enemy.velocity.y-GS*3);
        if(enemy.powerup == TRUE)
            [batch addChild:enemy z:2];
        else if(enemy.powerdown == TRUE)
            [batch addChild:enemy z:0];
        else
            [batch addChild:enemy z:1];
    }
}

-(void) checkForCollisions
{
	BasicMonster* enemy;
    Knight *knight = [[GameMechanics sharedGameMechanics] knight];
//    CGRect knightBoundingBox = [knight boundingBox];
    CGRect knightHitZone = [knight hitZone];
    
    // iterate over all enemies (all child nodes of this enemy batch)
	CCARRAY_FOREACH([batch children], enemy)
	{
        // only check for collisions if the enemy is visible
		if (enemy.visible)
		{
			CGRect bbox = [enemy boundingBox];
            /*
             1) check collision between Bounding Box of the knight and Bounding Box of the enemy.
                If a collision occurs here, only the knight can kill the enemy. The knight can not be injured by this colission. The knight can only be injured if his hit zone collides with an enemy (checked in step 2) )
             */
            // if we detect an intersection, a collision occured
//            if (CGRectIntersectsRect(knightBoundingBox, bbox))
//			{
//                // if the knight is stabbing, or the knight is in invincible mode, the enemy will be destroyed...
//                if (knight.stabbing == TRUE || knight.invincible)
//                {
//                    [enemy gotHit];
//                    // since the enemy was hit, we can skip the second check, using 'continue'.
//                    continue;
//                }
//			}
            /*
             2) now we check if the knights Hit Zone collided with the enemy. If this happens, and he is not stabbing, he will be injured.
             */
            if (CGRectIntersectsRect(knightHitZone, bbox))
            {
                // if the knight is stabbing, or the knight is in invincible mode, the enemy will be destroyed...
                if (knight.invincible || enemy.powerup == TRUE || enemy.powerdown == TRUE)
                {
                    [enemy gotHit];
                }
                else
                {
                    // if the kight is not stabbing, he will be hit
                    [knight gotHit];
                }
            }
		}
	}
    
    [self obstacleCollisions];
}

- (void)hideAllObjectsOnScreen
{
    BasicMonster * enemy;
    CCARRAY_FOREACH([batch children], enemy)
	{
        if(enemy.visible == TRUE)
            enemy.visible = FALSE;
    }
}

-(void) obstacleCollisions
{
    BasicMonster * enemy;
    CCARRAY_FOREACH([batch children], enemy)
    {
        BasicMonster * enemy2;
        CGRect bbox = [enemy boundingBox];
        if(enemy.powerup == FALSE && enemy.visible == TRUE && enemy.powerdown == FALSE)
        {
            CCARRAY_FOREACH([batch children], enemy2)
            {
                if(enemy2.powerup == FALSE && enemy2.visible == TRUE && enemy2.powerdown == FALSE)
                {
                    CGRect bbox2 = [enemy2 boundingBox];
                    if(CGRectIntersectsRect(bbox2, bbox) && (enemy != enemy2))
                    {
                        CGRect screenRect = [[UIScreen mainScreen] bounds];
                        if(enemy.position.y > enemy2.position.y && enemy.position.y  > screenRect.size.height)
                        {
                            enemy.visible = FALSE;
                            break;
                        }
                        else if(enemy2.position.y > enemy.position.y && enemy2.position.y  > screenRect.size.height)
                        {
                            enemy2.visible = FALSE;
                            break;
                        }
                        else if(enemy.velocity.x > 0)
                        {
                            enemy.position = ccp(enemy.position.x-1, enemy.position.y);
                            enemy.velocity = ccp(0, enemy.velocity.y);
                            break;
                        }
                        else if (enemy.velocity.x < 0)
                        {
                            enemy.position = ccp(enemy.position.x+1, enemy.position.y);
                            enemy.velocity = ccp(0, enemy.velocity.y);
                            break;
                        }
                        else if (enemy2.velocity.x > 0)
                        {
                            enemy2.position = ccp(enemy2.position.x-1, enemy2.position.y);
                            enemy2.velocity = ccp(0, enemy2.velocity.y);
                            break;
                        }
                        else if (enemy2.velocity.x < 0)
                        {
                            enemy2.position = ccp(enemy2.position.x+1, enemy2.position.y);
                            enemy2.velocity = ccp(0, enemy2.velocity.y);
                            break;
                        }
                        else if(enemy.position.y > enemy2.position.y)
                        {
                            //float GS = [[GameMechanics sharedGameMechanics]GAME_SPEED];
                            enemy.velocity = CGPointMake(0, enemy.velocity.y-5);
                            enemy2.velocity = CGPointMake(0, enemy2.velocity.y-7);
                            break;
                        }
                        else if(enemy2.position.y > enemy.position.y)
                        {
                            //float GS = [[GameMechanics sharedGameMechanics]GAME_SPEED];
                            enemy2.velocity = CGPointMake(0, enemy2.velocity.y-5);
                            enemy.velocity = CGPointMake(0, enemy.velocity.y-7);
                            break;
                        }
                    }
                }
            }
        }
    }
}

-(void) update:(ccTime)delta
{
    if([[GameMechanics sharedGameMechanics] bgChange] == TRUE)
    {
        [[GameMechanics sharedGameMechanics] setBgChange:FALSE];
        [self hideAllObjectsOnScreen];
    }

    
    // only execute the block, if the game is in 'running' mode
    if ([[GameMechanics sharedGameMechanics] gameState] == GameStateRunning)
    {
        updateCount++;
        
        // first we get all available monster types
        NSArray *monsterTypes = [[[GameMechanics sharedGameMechanics] spawnRatesByMonsterType] allKeys];
        
        for (Class monsterTypeClass in monsterTypes)
        {
            // we get the spawn frequency for this specific monster type
            int spawnFrequency = [[GameMechanics sharedGameMechanics] spawnRateForMonsterType:monsterTypeClass];
            
            // if the updateCount reached the spawnFrequency we spawn a new enemy
            if (updateCount % spawnFrequency == 0)
            {
                [self spawnEnemyOfType:monsterTypeClass];
            }
        }
        
        // speed up objects
        if([[GameMechanics sharedGameMechanics]GAME_SPEED] > [[GameMechanics sharedGameMechanics]currentGS] && [[GameMechanics sharedGameMechanics] speedUp] == TRUE)
        {
            BasicMonster * obs;
            CCARRAY_FOREACH([batch children], obs)
            {
                int xVel = obs.velocity.x;
                int yVel = obs.velocity.y;
                obs.velocity = CGPointMake(xVel, yVel-[[GameMechanics sharedGameMechanics]GAME_SPEED]*2);
            }
            float tempSpeed = [[GameMechanics sharedGameMechanics]currentGS];
            [[GameMechanics sharedGameMechanics] setCurrentGS:tempSpeed];
            [[GameMechanics sharedGameMechanics] setSpeedUp:FALSE];
        }
        
        // slow objects
        if([[GameMechanics sharedGameMechanics]distance] >= [[GameMechanics sharedGameMechanics] pick] + 100 && [[GameMechanics sharedGameMechanics] speedDown] == TRUE)
        {
            [[GameMechanics sharedGameMechanics] setBackGroundScrollSpeedX:SCROLL_SPEED_DEFAULT];
            [[[GameMechanics sharedGameMechanics] knight] setInvincible:FALSE];
            BasicMonster * obs;
            CCARRAY_FOREACH([batch children], obs)
            {
                int xVel = obs.velocity.x;
                int yVel = obs.velocity.y;
                obs.velocity = CGPointMake(xVel, yVel+[[GameMechanics sharedGameMechanics]GAME_SPEED]*2);
            }
            [[GameMechanics sharedGameMechanics] setGAME_SPEED:[[GameMechanics sharedGameMechanics]currentGS]];
            [[GameMechanics sharedGameMechanics] setSpeedDown:FALSE];
        }
        
        // change lanes
        if([[GameMechanics sharedGameMechanics] inTunnel] == FALSE)
        {
            if([[batch children]count] > 0 && [[batch children]count] < 100)
            {
                [self changeLane];
            }
        }
        
        [self changeLaneStop];
        [self checkForCollisions];
    }
}
-(void)changeLane
{
    
    BOOL collides = FALSE;
    int r = arc4random() % [[GameMechanics sharedGameMechanics] changeLaneRate];
    if(r == 0)
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        BasicMonster * obs;
        CCArray * obsArr = [batch children];
        r = arc4random() % obsArr.count;
        obs = [obsArr objectAtIndex:r];
        while(obs.powerup==TRUE)
        {
            r = arc4random() % obsArr.count;
            obs = [obsArr objectAtIndex:r];
        }
        r = arc4random() % 2;
        
        BasicMonster * temp;
        
        // check for future collision
        // going right
        int posX = obs.position.x;

        if(r == 2 && obs.powerup == FALSE && posX != 280 && obs.visible == TRUE && obs.powerdown == FALSE)
        {
            CCARRAY_FOREACH(obsArr, temp)
            {
                if(temp.visible == TRUE && obs.position.x + screenRect.size.width/4 != temp.position.x)
                {
                    int y1 = obs.position.y+obs.contentSize.height;
                    int y2 = obs.position.y;
                    int y3 = temp.position.y+temp.contentSize.height;
                    int y4 = temp.position.y;
                    
                    if((y2 < y3) && (y2 >y4) && (y1 >y4) && (y1<y3))
                    {
                        collides = TRUE;
                        obs.stopAt = obs.position.x + screenRect.size.width/4;
                    }
                }
            }
            
            if(collides == FALSE)
            {
                obs.direction = 0;
                int yVel = obs.velocity.y;
                obs.velocity = CGPointMake(30, yVel);
                obs.stopAt = obs.position.x - screenRect.size.width/4;
            }
        }
        //going left
        else if(r == 1 && obs.powerup == FALSE && posX !=40 && obs.visible == TRUE && obs.powerdown == FALSE)
        {
            CCARRAY_FOREACH(obsArr, temp)
            {
                if(temp.visible == TRUE && obs.position.x - screenRect.size.width/4 != temp.position.x)
                {
                    int y1 = obs.position.y+obs.contentSize.height;
                    int y2 = obs.position.y;
                    int y3 = temp.position.y+temp.contentSize.height;
                    int y4 = temp.position.y;
                    
                    if((y3>y1) && (y4<y1) && (y2<y3) && (y2<y4))
                    {
                        collides = TRUE;
                    }
                }
            }
            
            if(collides == FALSE)
            {
                obs.direction = 1;
                int yVel = obs.velocity.y;
                obs.velocity = CGPointMake(-30, yVel);
                obs.stopAt = obs.position.x - screenRect.size.width/4;
            }
        }
    }
}

-(void) changeLaneStop
{
    BasicMonster * obs;
    CCARRAY_FOREACH([batch children], obs)
    {
        int posX = obs.position.x;
        if(posX >= obs.stopAt && obs.stopAt != 0 && obs.direction == 0)
        {
            int yVel = obs.velocity.y;
            obs.stopAt = 0;
            obs.velocity = CGPointMake(0, yVel);
        }
        else if(posX <= obs.stopAt && obs.stopAt != 0 && obs.direction == 1)
        {
            int yVel = obs.velocity.y;
            obs.stopAt = 0;
            obs.velocity = CGPointMake(0, yVel);
        }
    }
}
@end
