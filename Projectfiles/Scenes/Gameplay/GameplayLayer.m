//
//  GameplayScene.m
//  _MGWU-SideScroller-Template_
//
//  Created by Benjamin Encz on 5/15/13.
//  Copyright (c) 2013 MakeGamesWithUs Inc. Free to use for all purposes.
//

#import "GameplayLayer.h"
#import "ParallaxBackground.h"
#import "Game.h"
#import "BasicMonster.h"
#import "SlowMonster.h"
#import "GameMechanics.h"
#import "EnemyCache.h"
#import "MainMenuLayer.h"
#import "Mission.h"
#import "RecapScreenScene.h"
#import "Store.h"
#import "PopupProvider.h"
#import "CCControlButton.h"
#import "StyleManager.h"
#import "NotificationBox.h"
#import "PauseScreen.h"
#import "speedup.h"
#import "UDO.h"
#import "miniPackage.h"
#import "oilSplat.h"
#import "teleport.h"
#import "train.h"
#import "pitHole.h"

// defines how many update cycles run, before the missions get an update about the current game state
#define MISSION_UPDATE_FREQUENCY 10

@interface GameplayLayer()
/*
 Tells the game to display the skipAhead button for N seconds.
 After the N seconds the button will automatically dissapear.
 */
- (void)presentSkipAheadButtonWithDuration:(NSTimeInterval)duration;

/*
 called when skipAheadButton has been touched
 */
- (void)skipAheadButtonPressed;

/*
 called when pause button was pressed
 */
- (void)pauseButtonPressed;

/*
 called when the player has chosen if he wants to continue the game (for paying coins) or not
 */
- (void)goOnPopUpButtonClicked:(CCControlButton *)sender;

@end

@implementation GameplayLayer

+ (id) sharedGamePlayLayer
{
    static dispatch_once_t once;
    static id sharedInstance;
    /*  Uses GCD (Grand Central Dispatch) to restrict this piece of code to only be executed once
     This code doesn't need to be touched by the game developer.
     */
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
+ (id)scene
{
    CCScene *scene = [CCScene node];
    GameplayLayer* layer = [GameplayLayer node];
    
    // By default we want to show the main menu
    layer.showMainMenu = TRUE;
    
    [scene addChild:layer];
    return scene;
}

+ (id)noMenuScene
{
    CCScene *scene = [CCScene node];
    GameplayLayer* layer = [GameplayLayer node];
    
    // By default we want to show the main menu
    layer.showMainMenu = FALSE;
    
    [scene addChild:layer];
    return scene;
}

#pragma mark - Initialization

- (void)dealloc
{
    /*
     When our object is removed, we need to unregister from all notifications.
     */
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:1];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:2];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"background_beat.wav"];
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"background_beat.wav" loop:TRUE];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"hit_car.aiff"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"bike_start.wav"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"tele.wav"];
        
        [[GameMechanics sharedGameMechanics] setPassed250:FALSE];
        // get screen center
        CGPoint screenCenter = [CCDirector sharedDirector].screenCenter;
        
        // preload particle effects
        // To preload the textures, play each effect once off-screen
        CCParticleSystem* system = [CCParticleSystemQuad particleWithFile:@"fx-explosion.plist"];
        system.positionType = kCCPositionTypeFree;
        system.autoRemoveOnFinish = YES;
        system.position = ccp(MAX_INT, MAX_INT);
        // adding it as child lets the particle effect play
        [self addChild:system];
        
        // add the scrolling background
        ParallaxBackground *background = [ParallaxBackground node];
        [self addChild:background z:-2];
        
        hudNode = [CCNode node];
        [self addChild:hudNode z:MAX_INT-2];
        
        // add the knight
        knight = [[Knight alloc] initWithKnightPicture];
        [self addChild:knight];
        knight.anchorPoint = ccp(0,0);
        
        // add the health display
        healthDisplayNode = [[HealthDisplayNode alloc] initWithHealthImage:@"heart_filled.png" lostHealthImage:@"heart_empty.png" maxHealth:1];
        [hudNode addChild:healthDisplayNode z:MAX_INT-1];
        healthDisplayNode.position = ccp(screenCenter.x, self.contentSize.height - 18);
        
        // add scoreboard entry for coins
        coinsDisplayNode = [[ScoreboardEntryNode alloc] initWithScoreImage:@"coin.png" fontFile:@"avenir.fnt"];
        coinsDisplayNode.scoreStringFormat = @"%d";
        coinsDisplayNode.position = ccp(20, self.contentSize.height - 26);
        [hudNode addChild:coinsDisplayNode z:MAX_INT-1];
        
        // add scoreboard entry for in-app currency
        inAppCurrencyDisplayNode = [[ScoreboardEntryNode alloc] initWithScoreImage:@"coin.png" fontFile:@"avenir.fnt"];
        inAppCurrencyDisplayNode.scoreStringFormat = @"%d";
        inAppCurrencyDisplayNode.position = ccp(self.contentSize.width - 120, self.contentSize.height - 26);
        inAppCurrencyDisplayNode.score = [Store availableAmountInAppCurrency];
        [hudNode addChild:inAppCurrencyDisplayNode z:MAX_INT-1];
        
        // add scoreboard entry for points
        pointsDisplayNode = [[ScoreboardEntryNode alloc] initWithScoreImage:nil fontFile:@"avenir24.fnt"];
        pointsDisplayNode.position = ccp(10, self.contentSize.height - 50);
        pointsDisplayNode.scoreStringFormat = @"%d m";
        [hudNode addChild:pointsDisplayNode z:MAX_INT-1];
        
        // set up the skip ahead menu
        CCSprite *skipAhead = [CCSprite spriteWithFile:@"skipahead.png"];
        CCSprite *skipAheadSelected = [CCSprite spriteWithFile:@"skipahead-pressed.png"];
        skipAheadMenuItem = [CCMenuItemSprite itemWithNormalSprite:skipAhead selectedSprite:skipAheadSelected target:self selector:@selector(skipAheadButtonPressed)];
        skipAheadMenu = [CCMenu menuWithItems:skipAheadMenuItem, nil];
        skipAheadMenu.position = ccp(self.contentSize.width - skipAheadMenuItem.contentSize.width -20, self.contentSize.height - 80);
        // initially skipAheadMenu is hidden
        skipAheadMenu.visible = FALSE;
        [hudNode addChild:skipAheadMenu];
        
        // set up pause button
        CCSprite *pauseButton = [CCSprite spriteWithFile:@"pause.png"];
        CCSprite *pauseButtonPressed = [CCSprite spriteWithFile:@"pause-pressed.png"];
        pauseButtonMenuItem = [CCMenuItemSprite itemWithNormalSprite:pauseButton selectedSprite:pauseButtonPressed target:self selector:@selector(pauseButtonPressed)];
        pauseButtonMenu = [CCMenu menuWithItems:pauseButtonMenuItem, nil];
        pauseButtonMenu.position = ccp(self.contentSize.width - pauseButtonMenuItem.contentSize.width - 4, self.contentSize.height - 58);
        [hudNode addChild:pauseButtonMenu];
        
        // add the enemy cache containing all spawned enemies
        [self addChild:[EnemyCache node]];
        
        // setup a new gaming session
        [self resetGame];
        
        [self scheduleUpdate];
        
        /**
         A Notification can be used to broadcast an information to all objects of a game, that are interested in it.
         Here we sign up for the 'GamePaused' and 'GameResumed' information, that is broadcasted by the GameMechanics class. Whenever the game pauses or resumes, we get informed and can react accordingly.
         **/
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gamePaused) name:@"GamePaused" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameResumed) name:@"GameResumed" object:nil];
    }
    
    return self;
}

- (void)gamePaused
{
    [self pauseSchedulerAndActions];
}

- (void)gameResumed
{
    [self resumeSchedulerAndActions];
}

#pragma mark - Reset Game

- (void)startGame
{
    //[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"background_beat.wav" loop:TRUE];
    [[SimpleAudioEngine sharedEngine] playEffect:@"bike_start.wav"];
    [[GameMechanics sharedGameMechanics] setGameState:GameStateRunning];
    [self enableGamePlayButtons];
    [self presentSkipAheadButtonWithDuration:5.f];
    [[GameMechanics sharedGameMechanics] setChangeLaneRate:DEFAULT_changeLaneRate];
    [[GameMechanics sharedGameMechanics] setPickPD:0];
    [[GameMechanics sharedGameMechanics] setSensitivity:DEFAULT_sensitivity];
    [[GameMechanics sharedGameMechanics] setMaxVel:DEFAULT_maxVelocity];
    [[GameMechanics sharedGameMechanics] setBgChange:FALSE];
    [[GameMechanics sharedGameMechanics] setInTunnel:FALSE];
    [[GameMechanics sharedGameMechanics] setOilHit:FALSE];
    [[GameMechanics sharedGameMechanics] setLane1taken:FALSE];
    [[GameMechanics sharedGameMechanics] setLane2taken:FALSE];
    [[GameMechanics sharedGameMechanics] setLane3taken:FALSE];
    [[GameMechanics sharedGameMechanics] setPickPD:FALSE];
    [[GameMechanics sharedGameMechanics] setPowerDownType:FALSE];
    
    /*
     inform all missions, that they have started
     */
    for (Mission *m in game.missions)
    {
        [m missionStart:game];
    }
}

- (void)resetGame
{
    [[GameMechanics sharedGameMechanics] resetGame];

    [[GameMechanics sharedGameMechanics] setGAME_SPEED:DEFAULT_GAME_SPEED];
    [[GameMechanics sharedGameMechanics] setCurrentGS:DEFAULT_GAME_SPEED];
    [[GameMechanics sharedGameMechanics] setSpeedUp:FALSE];
    [[GameMechanics sharedGameMechanics] setSpeedDown:FALSE];
    [[GameMechanics sharedGameMechanics] setPrevDist:125.0f];
    [[GameMechanics sharedGameMechanics] setPowerUpType:0];
    [[GameMechanics sharedGameMechanics] setSpawnRate:1.0f];
    [[GameMechanics sharedGameMechanics] setPassed250:FALSE];
    [[GameMechanics sharedGameMechanics] setPoweredDown:FALSE];
    [[GameMechanics sharedGameMechanics] setPoweredUp:FALSE];
    [[GameMechanics sharedGameMechanics] setBgChange:FALSE];
    [[GameMechanics sharedGameMechanics] setInTunnel:FALSE];
    [[GameMechanics sharedGameMechanics] setOilHit:FALSE];
    [[GameMechanics sharedGameMechanics] setLane1taken:FALSE];
    [[GameMechanics sharedGameMechanics] setLane2taken:FALSE];
    [[GameMechanics sharedGameMechanics] setLane3taken:FALSE];
    [[GameMechanics sharedGameMechanics] setPickPD:FALSE];
    [[GameMechanics sharedGameMechanics] setPowerDownType:FALSE];
    
    game = [[Game alloc] init];
    [[GameMechanics sharedGameMechanics] setGame:game];
    [[GameMechanics sharedGameMechanics] setKnight:knight];
    // add a reference to this gamePlay scene to the gameMechanics, which allows accessing the scene from other classes
    [[GameMechanics sharedGameMechanics] setGameScene:self];

    // set the default background scroll speed
    [[GameMechanics sharedGameMechanics] setBackGroundScrollSpeedX:SCROLL_SPEED_DEFAULT];
    
    /* setup initial values */
    
    // setup knight
    knight.position = ccp(140,20);
    knight.zOrder = 10;
    knight.hitPoints = KNIGHT_HIT_POINTS;
    
    // setup HUD
    healthDisplayNode.health = knight.hitPoints;
    coinsDisplayNode.score = game.score;
    pointsDisplayNode.score = game.meters;
    
    // set spwan rate for monsters
    [[GameMechanics sharedGameMechanics] setSpawnRate:150 forMonsterType:[BasicMonster class]];
    [[GameMechanics sharedGameMechanics] setSpawnRate:150 forMonsterType:[SlowMonster class]];
    [[GameMechanics sharedGameMechanics] setSpawnRate:750 forMonsterType:[miniPackage class]];
    
    // set gravity (used for jumps)
    [[GameMechanics sharedGameMechanics] setWorldGravity:ccp(0.f, 0.f)]; //ydefault -750.f
    
    // set the floor height, this will be the minimum y-Position for all entities
    [[GameMechanics sharedGameMechanics] setFloorHeight:0.f];
}

#pragma mark - Update & Input Events

-(void) accelerometer:(UIAccelerometer *)accelerometer
        didAccelerate:(UIAcceleration *)acceleration
{
	// controls how quickly velocity decelerates (lower = quicker to change direction)
	float deceleration = 0.1f;
	// determines how sensitive the accelerometer reacts (higher = more sensitive)
	float sensitivity = [[GameMechanics sharedGameMechanics] sensitivity];
	// how fast the velocity can be at most
	float maxVelocity = [[GameMechanics sharedGameMechanics] maxVel];
    	
	// adjust velocity based on current accelerometer acceleration
	float velocityX = knight.velocity.x * deceleration + acceleration.x * sensitivity;
    float velocityY = knight.velocity.y * deceleration + acceleration.y * sensitivity;
	
	// we must limit the maximum velocity of the player sprite, in both directions
	if (knight.velocity.x > maxVelocity)
		velocityX = maxVelocity;

    else if (knight.velocity.x < - maxVelocity)
		velocityX = - maxVelocity;
    
    if (knight.velocity.y < - maxVelocity)
        velocityY = -maxVelocity;
    
    else if (knight.velocity.y > maxVelocity)
        velocityY = maxVelocity;
    
    knight.velocity = ccp(velocityX, velocityY);
}

- (void)pushGameStateToMissions
{
    for (Mission *mission in game.missions)
    {
        if (mission.successfullyCompleted)
        {
            // we skip this mission, since it succesfully completed
            continue;
        }
        
        if ([mission respondsToSelector:@selector(generalGameUpdate:)])
        {
            // inform about current game state
            [mission generalGameUpdate:game];
            
            // check if mission is now completed
            if (mission.successfullyCompleted)
            {
                NSString *missionAccomplished = [NSString stringWithFormat:@"Completed: %@", mission.missionDescription];
                [NotificationBox presentNotificationBoxOnNode:self withText:missionAccomplished duration:1.f];
            }
        }
    }
}

- (void) update:(ccTime)delta
{
    // update the amount of in-App currency in pause mode, too
    inAppCurrencyDisplayNode.score = [Store availableAmountInAppCurrency];
    
    if ([[GameMechanics sharedGameMechanics] gameState] == GameStateRunning)
    {
        [self updateRunning:delta];
        
        // check if we need to inform missions about current game state
        updateCount++;
        if ((updateCount % MISSION_UPDATE_FREQUENCY) == 0)
        {
            [self pushGameStateToMissions];
        }
    }
    
}

- (void)updateRunning:(ccTime)delta
{
//    if([[GameMechanics sharedGameMechanics] bgChange] == TRUE && [[GameMechanics sharedGameMechanics] inTunnel] == TRUE)
//    {
//        //[[GameMechanics sharedGameMechanics] setBgChange:FALSE];
//        //[[[GameMechanics sharedGameMechanics] spawnRatesByMonsterType] removeAllObjects];
//        [[GameMechanics sharedGameMechanics] setSpawnRate:0 forMonsterType:[BasicMonster class]];
//        [[GameMechanics sharedGameMechanics] setSpawnRate:0 forMonsterType:[SlowMonster class]];
//        [[GameMechanics sharedGameMechanics] setSpawnRate:0 forMonsterType:[UDO class]];
//    }
//    else if([[GameMechanics sharedGameMechanics] bgChange] == TRUE && [[GameMechanics sharedGameMechanics] inTunnel] == FALSE)
//            //&& [[GameMechanics sharedGameMechanics] spawnNorm] == TRUE)
//    {
//        
//        //[[GameMechanics sharedGameMechanics] setBgChange:FALSE];
//        //[[[GameMechanics sharedGameMechanics] spawnRatesByMonsterType] removeAllObjects];
//        [[GameMechanics sharedGameMechanics] setSpawnRate:150 forMonsterType:[BasicMonster class]];
//        [[GameMechanics sharedGameMechanics] setSpawnRate:150 forMonsterType:[SlowMonster class]];
//        [[GameMechanics sharedGameMechanics] setSpawnRate:200 forMonsterType:[UDO class]];
//    }
    
    // distance depends on the current scrolling speed
    self.gainedDistance += (delta * [[GameMechanics sharedGameMechanics] backGroundScrollSpeedX]) / 5;
    game.meters = (int) (self.gainedDistance);
    // update the score display
    pointsDisplayNode.score = game.meters;
    coinsDisplayNode.score = game.score;
    healthDisplayNode.health = knight.hitPoints;
    GameMechanics * dist = [GameMechanics sharedGameMechanics];
    dist.distance = self.gainedDistance;
    
    // the gesture recognizer assumes portrait mode, so we need to use rotated swipe directions
    KKInput* input = [KKInput sharedInput];

//    if ([input gestureSwipeRecognizedThisFrame])
//    {
//        KKSwipeGestureDirection dir = input.gestureSwipeDirection;
//        // TODO: this needs to be fixed, kobold 2d autotransforms the touches, depending on the device orientation, which is no good for games not supporting all orientations
//        switch (dir)
//        {
//            case KKSwipeGestureDirectionUp:
//                [knight jump];
//                break;
//            case KKSwipeGestureDirectionRight:
//                [knight stab];
//                break;
//            default:
//                break;
//        }
//    }
    
    if([[GameMechanics sharedGameMechanics] powerUpType] == 2)
    {
        [[KKInput sharedInput] setGestureTapEnabled:TRUE];
        if(input.gestureTapRecognizedThisFrame)
        {
            [[SimpleAudioEngine sharedEngine]playEffect:@"tele.wav"];
            int x = input.gestureTapLocation.x;
            int y = input.gestureTapLocation.y;
            knight.position = ccp(x-knight.contentSize.width/2, y-knight.contentSize.height/2);
        }
    }
    
    // show notification after picking up a powerup or power down
    if([[GameMechanics sharedGameMechanics] poweredUp]==TRUE && [[GameMechanics sharedGameMechanics] powerUpType] == 1)
    {
        [NotificationBox presentNotificationBoxOnNode:self withText:@"Lets Rock!" duration:4.5f];
        [[GameMechanics sharedGameMechanics]setPoweredUp:FALSE];
    }
    if([[GameMechanics sharedGameMechanics] poweredUp]==TRUE && [[GameMechanics sharedGameMechanics] powerUpType] == 2)
    {
        [NotificationBox presentNotificationBoxOnNode:self withText:@"Tap to teleport!" duration:4.5f];
        [[GameMechanics sharedGameMechanics]setPoweredUp:FALSE];
    }
    
    if([[GameMechanics sharedGameMechanics] poweredDown]==TRUE && [[GameMechanics sharedGameMechanics] powerDownType] == 1)
    {
        [NotificationBox presentNotificationBoxOnNode:self withText:@"You Lost Traction!" duration:4.5f];
        [[GameMechanics sharedGameMechanics]setPoweredDown:FALSE];
    }
    
    // spawn another object at X distance
    if([[GameMechanics sharedGameMechanics] distance] > 250.0f && [[GameMechanics sharedGameMechanics] passed250] ==  FALSE)
    {
        [NotificationBox presentNotificationBoxOnNode:self withText:@"New things are on its way!" duration:4.5f];
        [[GameMechanics sharedGameMechanics] setSpawnRate:200 forMonsterType:[UDO class]];
        [[GameMechanics sharedGameMechanics] setSpawnRate:750 forMonsterType:[speedup class]];
        [[GameMechanics sharedGameMechanics] setSpawnRate:200 forMonsterType:[oilSplat class]];
        [[GameMechanics sharedGameMechanics] setSpawnRate:750 forMonsterType:[teleport class]];
        [[GameMechanics sharedGameMechanics] setSpawnRate:500 forMonsterType:[pitHole class]];
        [[GameMechanics sharedGameMechanics] setPassed250:TRUE];
    }
    
    //incrase speed & spawn rate, check if in tunnel
    if([[GameMechanics sharedGameMechanics] distance] > [[GameMechanics sharedGameMechanics]prevDist]*1.5 && [[GameMechanics sharedGameMechanics] passed250] == TRUE
       && [[GameMechanics sharedGameMechanics] inTunnel] == FALSE)
    {
        [self increaseSpawnRate];
        [self increaseCurrentGameSpeed];
    }
    
    // disable power down oil slick
    if([[GameMechanics sharedGameMechanics]pickPD] + 100 <= [[GameMechanics sharedGameMechanics]distance] && [[GameMechanics sharedGameMechanics] powerDownType] == 1)
    {
        [NotificationBox presentNotificationBoxOnNode:self withText:@"Oil Wore Off!" duration:1.0f];
        [[GameMechanics sharedGameMechanics] setSensitivity:DEFAULT_sensitivity];
        [[GameMechanics sharedGameMechanics] setMaxVel:DEFAULT_maxVelocity];
        [[GameMechanics sharedGameMechanics] setPowerDownType:0];
        [[GameMechanics sharedGameMechanics] setPickPD:0];
    }
    
    // disable power up teleport
    if([[GameMechanics sharedGameMechanics]pick] + 100 <= [[GameMechanics sharedGameMechanics]distance] && [[GameMechanics sharedGameMechanics] powerUpType] == 2)
    {
        [NotificationBox presentNotificationBoxOnNode:self withText:@"Teleport Disabled!" duration:1.0f];
        [[GameMechanics sharedGameMechanics] setPowerUpType:0];
        [[GameMechanics sharedGameMechanics] setPoweredUp:FALSE];
        input.gestureTapEnabled = FALSE;
        [[GameMechanics sharedGameMechanics]setPick:0];
    }
    
    if (knight.hitPoints == 0)
    {
        // knight died, present screen with option to GO ON for paying some coins
        [NotificationBox presentNotificationBoxOnNode:self withText:@"You broke your bike!" duration:4.5f];
        [[SimpleAudioEngine sharedEngine]playEffect:@"hit_car.aiff"];
        [self presentGoOnPopUp];
    }
}

// increase monster spawn rate
-(void)increaseSpawnRate
{
    int sr = [[GameMechanics sharedGameMechanics] spawnRateForMonsterType:[BasicMonster class]];
    [[GameMechanics sharedGameMechanics] setSpawnRate:(sr-10) forMonsterType:[BasicMonster class]];
    [[GameMechanics sharedGameMechanics] setSpawnRate:(sr-10) forMonsterType:[SlowMonster class]];
    [[GameMechanics sharedGameMechanics] setSpawnRate:(sr-5) forMonsterType:[UDO class]];
}

// speed up the game play
-(void)increaseCurrentGameSpeed
{
    float GS = [[GameMechanics sharedGameMechanics]GAME_SPEED];
    float BGSS = [[GameMechanics sharedGameMechanics]backGroundScrollSpeedX];
    float PD = [[GameMechanics sharedGameMechanics] prevDist];
    [[GameMechanics sharedGameMechanics] setGAME_SPEED:(GS+1.f)];
    [[GameMechanics sharedGameMechanics] setBackGroundScrollSpeedX:(BGSS+10.0f)];
    [[GameMechanics sharedGameMechanics] setPrevDist:(PD*1.5)];
    //[[GameMechanics sharedGameMechanics] setSpeedUp:TRUE];
}

#pragma mark - Scene Lifecycle

- (void)onEnterTransitionDidFinish
{
    // setup a gesture listener for jumping and stabbing gestures
    [KKInput sharedInput].gestureSwipeEnabled = TRUE;
    // register for accelerometer input, to controll the knight
    self.accelerometerEnabled = YES;
    if (self.showMainMenu)
    {
        // add main menu
        MainMenuLayer *mainMenuLayer = [[MainMenuLayer alloc] init];
        [self addChild:mainMenuLayer z:MAX_INT];
    } else
    {
        // start game directly
        [self showHUD:TRUE];
        [self startGame];
    }
}

- (void)onExit
{
    // very important! deactivate the gestureInput, otherwise touches on scrollviews and tableviews will be cancelled!
    [KKInput sharedInput].gestureSwipeEnabled = FALSE;
    [KKInput sharedInput].gestureTapEnabled = FALSE;
    self.accelerometerEnabled = FALSE;
}

#pragma mark - UI

- (void)presentGoOnPopUp
{
    
//    [[GameMechanics sharedGameMechanics] setGameState:GameStatePaused];
//    // TODO: temp fix to bypass lock
//
//    // Cancel button selected
//    [goOnPopUp dismiss];
//    game.score ++;
//    
//    // IMPORTANT: set game state to 'GameStateMenu', otherwise menu animations will no be played
//    [[GameMechanics sharedGameMechanics] setGameState:GameStateMenu];
//
//    RecapScreenScene *recap = [[RecapScreenScene alloc] initWithGame:game];
//    [[CCDirector sharedDirector] replaceScene:recap];
//
//    
//    CCScale9Sprite *backgroundImage = [StyleManager goOnPopUpBackground];
//
//    goOnPopUp = [PopupProvider presentPopUpWithContentString:nil backgroundImage:backgroundImage target:self selector:@selector(goOnPopUpButtonClicked:) buttonTitles:@[@"OK", @"No"]];
//    [self disableGameplayButtons];
    [[KKInput sharedInput] setGestureTapEnabled:FALSE];
    [[GameMechanics sharedGameMechanics] setGameState:GameStatePaused];
    CCScale9Sprite *backgroundImage = [StyleManager goOnPopUpBackground];
    goOnPopUp = [PopupProvider presentPopUpWithContentString:nil backgroundImage:backgroundImage target:self selector:@selector(goOnPopUpButtonClicked:) buttonTitles:@[@"Yes", @"No"]];
    [self disableGameplayButtons];
    
}

- (void)showHUD:(BOOL)animated
{
    // TODO: implement animated
    hudNode.visible = TRUE;
}

- (void)hideHUD:(BOOL)animated
{
    // TODO: implement animated
    hudNode.visible = FALSE;
}

- (void)presentSkipAheadButtonWithDuration:(NSTimeInterval)duration
{
    skipAheadMenu.visible = TRUE;
    skipAheadMenu.opacity = 0.f;
    CCFadeIn *fadeIn = [CCFadeIn actionWithDuration:0.5f];
    [skipAheadMenu runAction:fadeIn];

    [self scheduleOnce: @selector(hideSkipAheadButton) delay:duration];
}

- (void)hideSkipAheadButton
{
    CCFiniteTimeAction *fadeOut = [CCFadeOut actionWithDuration:1.5f];
    CCCallBlock *hide = [CCCallBlock actionWithBlock:^{
        // execute this code to hide the 'Skip Ahead'-Button, once it is faded out
        skipAheadMenu.visible = FALSE;
        // enable again, so that interaction is possible as soon as the menu is visible again
        skipAheadMenu.enabled = TRUE;
    }];

    CCSequence *fadeOutAndHide = [CCSequence actions:fadeOut, hide, nil];
    
    [skipAheadMenu runAction:fadeOutAndHide];
}

- (void)disableGameplayButtons
{
    pauseButtonMenu.enabled = FALSE;
    skipAheadMenu.enabled = FALSE;
}
- (void)enableGamePlayButtons
{
    pauseButtonMenu.enabled = TRUE;
    skipAheadMenu.enabled = TRUE;
}

#pragma mark - Delegate Methods

/* 
 This method is called, when purchases on the In-Game-Store occur.
 Then we need to update the coins display on the HUD.
 */
- (void)storeDisplayNeedsUpdate
{
    inAppCurrencyDisplayNode.score = [Store availableAmountInAppCurrency];
}

- (void)resumeButtonPressed:(PauseScreen *)pauseScreen
{
    // enable the pause button again, since the pause menu is hidden now
    [self enableGamePlayButtons];
}

#pragma mark - Button Callbacks

- (void)skipAheadButtonPressed
{
    if ([Store hasSufficientFundsForSkipAheadAction])
    {    
        skipAheadMenu.enabled = FALSE;
        CCLOG(@"Skip Ahead!");
        [self startSkipAheadMode];
        
        // hide the skip ahead button, after it was pressed
        // we need to cancel the previously scheduled perform selector...
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideSkipAheadButton) object:nil];
        
        // ... in order to execute it directly
        [self hideSkipAheadButton];
    } else
    {
        // pause the game, to allow the player to buy coins 
//        [[GameMechanics sharedGameMechanics] setGameState:GameStatePaused];
//        [self presentMoreCoinsPopUpWithTarget:self selector:@selector(returnedFromMoreCoinsScreenFromSkipAheadAction)];
        [NotificationBox presentNotificationBoxOnNode:self withText:@"Not Enough Gold" duration:1.0f];
    }
}

- (void)goOnPopUpButtonClicked:(CCControlButton *)sender
{
    CCLOG(@"Button clicked.");
    if (sender.tag == 0)
    {
        if ([Store hasSufficientFundsForGoOnAction])
        {
            // OK button selected
            [[[GameMechanics sharedGameMechanics] knight]setPosition:ccp(150, 40)];
            
            int dist = [[GameMechanics sharedGameMechanics] distance];
            [[GameMechanics sharedGameMechanics] setPick:dist];
            [[GameMechanics sharedGameMechanics] setSpeedUp:TRUE];
            [[GameMechanics sharedGameMechanics] knight].visible = TRUE;
            [[GameMechanics sharedGameMechanics] knight].invincible = TRUE;
            [[GameMechanics sharedGameMechanics] setPowerUpType:1];
            [[GameMechanics sharedGameMechanics] setPoweredUp:TRUE];
            
            [goOnPopUp dismiss];
            [self executeGoOnAction];
            [self enableGamePlayButtons];
        } else
        {
            // game is paused in this state already, we only need to present the more coins screen
            // dismiss the popup; presented again when we return from the 'Buy More Coins'-Screen
//            [goOnPopUp dismiss];
//            [self presentMoreCoinsPopUpWithTarget:self selector:@selector(returnedFromMoreCoinsScreenFromGoOnAction)];
            // Cancel button selected
//            [goOnPopUp dismiss];
//            game.score ++;
//            
//            // IMPORTANT: set game state to 'GameStateMenu', otherwise menu animations will no be played
//            [[GameMechanics sharedGameMechanics] setGameState:GameStateMenu];
//            
//            RecapScreenScene *recap = [[RecapScreenScene alloc] initWithGame:game];
//            [[CCDirector sharedDirector] replaceScene:recap];
            [goOnPopUp dismiss];
            [self hitOkButNoGold];
        }
    } else if (sender.tag == 1)
    {
        // Cancel button selected
        [goOnPopUp dismiss];
        game.score ++;
        
        // IMPORTANT: set game state to 'GameStateMenu', otherwise menu animations will no be played
        [[GameMechanics sharedGameMechanics] setGameState:GameStateMenu];
        
        RecapScreenScene *recap = [[RecapScreenScene alloc] initWithGame:game];
        [[CCDirector sharedDirector] replaceScene:recap];
    }
}

-(void) hitOkButNoGold
{
    [NotificationBox presentNotificationBoxOnNode:self withText:@"Need 200 Gold to continue!" duration:1.f];
    [self presentGoOnPopUp];
}

- (void)pauseButtonPressed
{
    CCLOG(@"Pause");
    // disable pause button while the pause menu is shown, since we want to avoid, that the pause button can be hit twice. 
    [self disableGameplayButtons];
    
    PauseScreen *pauseScreen = [[PauseScreen alloc] initWithGame:game];
    pauseScreen.delegate = self;
    [self addChild:pauseScreen z:10];
    [pauseScreen present];
    [[GameMechanics sharedGameMechanics] setGameState:GameStatePaused];
}

#pragma mark - Game Logic

- (void)executeGoOnAction
{
    [Store purchaseGoOnAction];
    knight.hitPoints = KNIGHT_HIT_POINTS;
    [[GameMechanics sharedGameMechanics] setGameState:GameStateRunning];
    [NotificationBox presentNotificationBoxOnNode:self withText:@"Going on!" duration:1.f];
}

- (void)startSkipAheadMode
{
    BOOL successful = [Store purchaseSkipAheadAction];
    
    /* 
     Only enter the skip ahead mode if the purchase was successful (player had enough coins).
     This is checked previously, but we want to got sure that the player can never access this item
     without paying.
     */
    if (successful)
    {
        [[GameMechanics sharedGameMechanics] setBackGroundScrollSpeedX:SCROLL_SPEED_SKIP_AHEAD];
        [[GameMechanics sharedGameMechanics] knight].invincible = TRUE;
        [self scheduleOnce: @selector(endSkipAheadMode) delay:5.f];
        
        // present a notification, to inform the user, that he is in skip ahead mode
        [NotificationBox presentNotificationBoxOnNode:self withText:@"Skip Ahead Mode!" duration:4.5f];
    }
}

- (void)endSkipAheadMode
{
    [[GameMechanics sharedGameMechanics] setBackGroundScrollSpeedX:SCROLL_SPEED_DEFAULT];
    [[GameMechanics sharedGameMechanics] knight].invincible = FALSE;
}

- (void)presentMoreCoinsPopUpWithTarget:(id)target selector:(SEL)selector
{
    CCLOG(@"You need more coins!");
    NSArray *inGameStoreItems = [Store inGameStoreStoreItems];
    
    /* 
     The inGameStore is initialized with a callback method, which is called,
     once the closebutton is pressed.
     */
    inGameStore = [[InGameStore alloc] initWithStoreItems:inGameStoreItems backgroundImage:@"InGameStore_background.png" closeButtonImage:@"InGameStore_close.png" target:target selector:selector];
    /* The delegate adds a further callback, called when Items are purchased on the store, and we need to update our coin display */
    inGameStore.delegate = self;
    
    inGameStore.position = ccp(self.contentSize.width / 2, self.contentSize.height + 0.5 * inGameStore.contentSize.height);
    CGPoint targetPosition = ccp(self.contentSize.width / 2, self.contentSize.height /2);
    
    CCMoveTo *moveTo = [CCMoveTo actionWithDuration:1.f position:targetPosition];
    [self addChild:inGameStore z:MAX_INT];
    
    [inGameStore runAction:moveTo];
}


/* called when the 'More Coins Screen' has been closed, after previously beeing opened by
   attempting to buy a 'Skip Ahead' action */
- (void)returnedFromMoreCoinsScreenFromSkipAheadAction
{
    // hide store and resume game
    [inGameStore removeFromParent];
    [[GameMechanics sharedGameMechanics] setGameState:GameStateRunning];
    [self enableGamePlayButtons];
}

- (void)returnedFromMoreCoinsScreenFromGoOnAction
{
    [inGameStore removeFromParent];
    [self presentGoOnPopUp];
}

@end
