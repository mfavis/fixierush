//
//  GhostMonster.h
//  _MGWU-SideScroller-Template_
//
//  Created by Benjamin Encz on 5/16/13.
//  Copyright (c) 2013 MakeGamesWithUs Inc. Free to use for all purposes.
//

#import "BasicMonster.h"
#import "SimpleAudioEngine.h"

/**
 Class for ghost enemy.
 */

@interface BasicMonster : CCSprite

@property (nonatomic, assign) BOOL visible;
@property (nonatomic, assign) NSInteger hitPoints;
// velocity in pixels per second
@property (nonatomic, assign) CGPoint velocity;

@property (nonatomic, strong) NSMutableArray *animationFrames;
@property (nonatomic, strong) CCAction *run;
@property (nonatomic, assign) NSInteger initialHitPoints;
@property (nonatomic, assign) int stopAt;
@property BOOL powerup;
@property BOOL powerdown;
//0 is right
//1 is left
@property (nonatomic, assign) int direction;

- (id)initWithMonsterPicture;
- (void)spawn;
- (void)gotHit;

@end
