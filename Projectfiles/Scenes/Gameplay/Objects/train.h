//
//  train.h
//  FixieRush
//
//  Created by Mark Favis on 7/18/13.
//  Copyright (c) 2013 MakeGamesWithUs Inc. All rights reserved.
//

#import "BasicMonster.h"

@interface train : BasicMonster

// 0 up, 1 down
@property (nonatomic, assign) int direction ;

@end
