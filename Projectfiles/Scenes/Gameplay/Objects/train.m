//
//  train.m
//  FixieRush
//
//  Created by Mark Favis on 7/18/13.
//  Copyright (c) 2013 MakeGamesWithUs Inc. All rights reserved.
//

#import "train.h"
#import "GameMechanics.h"

@implementation train

- (id)initWithMonsterPicture
{
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"8-bit-bump.aiff"];
    self = [super initWithSpriteFrameName:@"monster8_1.png"];
    self.powerup = FALSE;
//    GameMechanics * speed = [GameMechanics sharedGameMechanics];
    
    if (self)
    {
        self.initialHitPoints = 1;
		self.velocity = CGPointMake(0, 0);
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"monsters.plist"];
        
        self.animationFrames = [NSMutableArray array];
        
        for(int i = 1; i <= 2; ++i)
        {
            [self.animationFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"monster8_%d.png", i]]];
        }
        
        //Create an animation from the set of frames you created earlier
        CCAnimation *running = [CCAnimation animationWithSpriteFrames: self.animationFrames delay:0.2f];
        
        //Create an action with the animation that can then be assigned to a sprite
        self.run = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:running]];
        
        // run the animation
        [self runAction:self.run];
        
        [self scheduleUpdate];
    }
    
    return self;
}

- (void)spawn
{
    if([self noLaneFree] == FALSE)
    {
        // Select a spawn location just outside the top side of the screen, with random lane position
        CGRect screenRect = [[CCDirector sharedDirector] screenRect];
        CGSize spriteSize = [self contentSize];
        //float xPos = screenRect.size.width + spriteSize.width * 0.5f;
        //float yPos = CCRANDOM_0_1() * (0.25 * screenRect.size.height - spriteSize.height) + spriteSize.height * 0.5f;
        NSMutableArray * laneXpos = [[NSMutableArray alloc]init];
        
        // create x-axis spawn points
        if(screenRect.size.width < 361)
        {
            [laneXpos addObject:[NSNumber numberWithInteger:40]];
            [laneXpos addObject:[NSNumber numberWithInteger:(40+screenRect.size.width*(1.f/3.f))]];
            [laneXpos addObject:[NSNumber numberWithInteger:(40+screenRect.size.width*(2.f/3.f))]];
        }
        else
        {
            [laneXpos addObject:[NSNumber numberWithInteger:80]];
            [laneXpos addObject:[NSNumber numberWithInteger:(80+screenRect.size.width*(1.f/3.f))]];
            [laneXpos addObject:[NSNumber numberWithInteger:(80+screenRect.size.width*(2.f/3.f))]];
        }
        
        // get x axis position
        int r = arc4random() % 3;
        float xPos = [[laneXpos objectAtIndex:r] integerValue];
        
        while(![self isLaneFree:r])
        {
            int r = arc4random() % 3;
             xPos = [[laneXpos objectAtIndex:r] integerValue];
        }
        
        // pick spawn from top or bottom
        r = arc4random() % 2;
        float yPos;
        
        if(r == 1)
            yPos = screenRect.size.height + spriteSize.height;
        else
            yPos = -spriteSize.height;
        
        // input x y positions
        self.position = CGPointMake(xPos,yPos);
        
        // set train speed
        int cGS = [[GameMechanics sharedGameMechanics] currentGS];
        
        if(yPos < 0)
        {
            self.direction = 0;
            self.velocity = CGPointMake(0, cGS*20);
        }
        else
        {
            self.direction = 1;
            self.velocity = CGPointMake(0, -cGS*20);
        }
        
        // Finally set yourself to be visible, this also flag the enemy as "in use"
        self.visible = YES;
        
        // mark a taken lane
        [self takeLane:TRUE];
        
        // reset health
        self.hitPoints = self.initialHitPoints;

    }
}


- (void)gotHit {
    CCParticleSystem* system = [CCParticleSystemQuad particleWithFile:@"fx-explosion.plist"];
    
    // Set some parameters that can't be set in Particle Designer
    system.positionType = kCCPositionTypeFree;
    system.autoRemoveOnFinish = YES;
    system.position = self.position;
    
    // Add the particle effect to the GameScene, for these reasons:
    // - self is a sprite added to a spritebatch and will only allow CCSprite nodes (it crashes if you try)
    // - self is now invisible which might affect rendering of the particle effect
    // - since the particle effects are short lived, there is no harm done by adding them directly to the GameScene
    [[[GameMechanics sharedGameMechanics] gameScene] addChild:system];
    
    // mark as invisible and move off screen
    self.visible = FALSE;
    self.position = ccp(-MAX_INT, 0);
}

- (void)updateRunningMode:(ccTime)delta
{
    // apply background scroll speed
    float yVelocity = self.velocity.y;
    float backgroundScrollSpeedX = [[GameMechanics sharedGameMechanics] backGroundScrollSpeedX];
    
    if(self.direction == 0)
    {
        yVelocity += (backgroundScrollSpeedX*2 + [[GameMechanics sharedGameMechanics]GAME_SPEED]*3);
        CGPoint combinedVelocity = ccp(self.velocity.x,yVelocity);
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        
        // move the monster until it leaves the top of the screen
        if (self.position.y < screenRect.size.height + self.contentSize.height)
        {
            [self setPosition:ccpAdd(self.position, ccpMult(combinedVelocity,delta))];

            if(self.position.y > screenRect.size.height + self.contentSize.height)
            {
                self.visible = FALSE;
                [self takeLane:FALSE];
            }
        }
    }

    else
    {
        yVelocity -= (backgroundScrollSpeedX*2 - [[GameMechanics sharedGameMechanics]GAME_SPEED]*3);
        CGPoint combinedVelocity = ccp(self.velocity.x,yVelocity);

        // move the monster until it leaves the bottom of the screen
        if (self.position.y > (self.contentSize.height * (-1)))
        {
            [self setPosition:ccpAdd(self.position, ccpMult(combinedVelocity,delta))];
            
            if(self.position.y < (0 - self.contentSize.height))
            {
                self.visible = FALSE;
                [self takeLane:FALSE];
            }
        }
    }
}

// mark that lane has been changed
- (void)takeLane: (BOOL)changeTo
{
    int xPos = self.position.x;
    CGRect screenRect = [[CCDirector sharedDirector] screenRect];
    
    if(screenRect.size.height < 481)
    {
        if(xPos < 41)
            [[GameMechanics sharedGameMechanics] setLane1taken:changeTo];
        else if(xPos > 41+screenRect.size.width*(2.f/3.f))
            [[GameMechanics sharedGameMechanics] setLane3taken:changeTo];
        else
            [[GameMechanics sharedGameMechanics] setLane2taken:changeTo];
    }
    else
    {
        if(xPos < 81)
            [[GameMechanics sharedGameMechanics] setLane1taken:changeTo];
        else if(xPos > 81+screenRect.size.width*(2.f/3.f))
            [[GameMechanics sharedGameMechanics] setLane3taken:changeTo];
        else
            [[GameMechanics sharedGameMechanics] setLane2taken:changeTo];
    }
}

- (BOOL)isLaneFree:(int)lane
{
    switch (lane) {
        case 0:
            if([[GameMechanics sharedGameMechanics] lane1taken] == FALSE)
                return TRUE;
        case 1:
            if([[GameMechanics sharedGameMechanics] lane2taken] == FALSE)
                return TRUE;
        case 2:
            if([[GameMechanics sharedGameMechanics] lane3taken] == FALSE)
                return TRUE;
        default:
            return FALSE;
    }
}
-(BOOL)noLaneFree
{
    int numFreeLane = 0;
    
    if([[GameMechanics sharedGameMechanics] lane1taken] == FALSE)
        numFreeLane++;
    if([[GameMechanics sharedGameMechanics] lane2taken] == FALSE)
        numFreeLane++;
    if([[GameMechanics sharedGameMechanics] lane3taken] == FALSE)
        numFreeLane++;
    
    if(numFreeLane == 0)
        return TRUE;
    else
        return FALSE;
}
@end
