//
//  oilSplat.m
//  FixieRush
//
//  Created by Mark Favis on 7/8/13.
//  Copyright (c) 2013 MakeGamesWithUs Inc. All rights reserved.
//

#import "GameMechanics.h"
#import "UDO.h"
#import "SlowMonster.h"
#import "oilSplat.h"
#import "NotificationBox.h"
#import "EnemyCache.h"

@implementation oilSplat

- (id)initWithMonsterPicture
{
    self = [super initWithSpriteFrameName:@"monster6_1.png"];
    self.powerup = FALSE;
    self.powerdown = TRUE;
    GameMechanics * speed = [GameMechanics sharedGameMechanics];
    if (self)
    {
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"powerDown.aiff"];
        self.initialHitPoints = 1;
		self.velocity = CGPointMake(0, -50*speed.GAME_SPEED);
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"monster-animations.plist"];
        
        self.animationFrames = [NSMutableArray array];
        
        for(int i = 1; i <= 2; ++i)
        {
            [self.animationFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"monster6_%d.png", i]]];
        }
        
        //Create an animation from the set of frames you created earlier
        CCAnimation *running = [CCAnimation animationWithSpriteFrames: self.animationFrames delay:0.2f];
        
        //Create an action with the animation that can then be assigned to a sprite
        self.run = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:running]];
        
        // run the animation
        [self runAction:self.run];
        
        [self scheduleUpdate];
    }
    
    return self;
}

- (void)gotHit {
    [[SimpleAudioEngine sharedEngine]playEffect:@"powerDown.aiff"];
    CCParticleSystem* system = [CCParticleSystemQuad particleWithFile:@"fx-explosion.plist"];
    
    // Set some parameters that can't be set in Particle Designer
    system.positionType = kCCPositionTypeFree;
    system.autoRemoveOnFinish = YES;
    system.position = self.position;
    
    // Add the particle effect to the GameScene, for these reasons:
    // - self is a sprite added to a spritebatch and will only allow CCSprite nodes (it crashes if you try)
    // - self is now invisible which might affect rendering of the particle effect
    // - since the particle effects are short lived, there is no harm done by adding them directly to the GameScene
    [[[GameMechanics sharedGameMechanics] gameScene] addChild:system];
    
    // mark as unvisible and move off screen
    self.visible = FALSE;
    self.position = ccp(-MAX_INT, 0);
    
    //[NotificationBox presentNotificationBoxOnNode:[GameplayLayer node] withText:@"You lost traction!" duration:1.0f];
    
    int dist = [[GameMechanics sharedGameMechanics] distance];
    [[GameMechanics sharedGameMechanics] setMaxVel:DEFAULT_sensitivity*3];
    [[GameMechanics sharedGameMechanics] setSensitivity:DEFAULT_maxVelocity*3];
    [[GameMechanics sharedGameMechanics] setPowerDownType:1];
    [[GameMechanics sharedGameMechanics] setPickPD:dist];
    [[GameMechanics sharedGameMechanics] setPoweredDown:TRUE];
    
//    if([[GameMechanics sharedGameMechanics] inTunnel] == FALSE)
//    {
//        [[GameMechanics sharedGameMechanics] setSpawnRate:0 forMonsterType:[BasicMonster class]];
//        [[GameMechanics sharedGameMechanics] setSpawnRate:0 forMonsterType:[SlowMonster class]];
//        [[GameMechanics sharedGameMechanics] setSpawnRate:0 forMonsterType:[UDO class]];
//        [[GameMechanics sharedGameMechanics] setBgChange:TRUE];
//        [[GameMechanics sharedGameMechanics] setOilHit:TRUE];
//
//    }
}

@end
